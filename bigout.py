import Utils as ut

import sys
import os
import glob

from ROOT import TH1F,TH2F,TH3F,TFile

dpath = '/global/cfs/cdirs/lz/users/wturner/MiscSimData/Gd152_realisticScint/lzap'
wfiles = '*.root'
figdir = '/global/project/projectdirs/lz/users/lkorley/odthreshold/figs'

zlist = glob.glob(dpath+'/'+wfiles)
#prepath = dpath+simtype+'_lzap_mctruth/'
#tlist = [prepath+os.path.basename(ipath)[:-5]+'_mctruth.root' for ipath in zlist]
myhists = ut.eventloop(zlist,pickleout='Gd152Hists2.p')
bigpde = myhists['pdeXYZ'].collapseAxis()
ahist = bigpde.toTHist()
myfile = TFile('Gd152_PDE.root','RECREATE')
myfile.cd()
ahist.Write()
myfile.Close()
