import uproot as up
import numpy as np
import awkward as awk
import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt
from pylab import rcParams
rcParams['figure.figsize'] = 22, 11
rcParams['font.size'] = 24
rcParams["savefig.format"] = 'png'
#rcParams['text.usetex'] = True

import sys
import os
import pickle

from ROOT import TH1F,TH2F,TH3F,TFile

def iterable(obj):
	try:
		iter(obj)
	except Exception:
		return False
	else:
		return True
	

def tfe(edges):
	diff = edges[1:] - edges[:-1]
	ticks = edges[:-1] + diff/2.0
	return ticks
	
class Histdd:
	def __init__(self,name,d,bins=[],ranges=[],axes=[]):
		self.edges = None
		self.H = None
		self.fresh = True
		self.savename=''
		self.name = name
		self.axes = axes
		self.ticks = []
		self.d = d
		self.bins = []
		self.nEntries = 0
		if len(bins)==1:
			self.bins = [bins[0] for a in range(self.d)]
		elif len(bins)>=self.d:
			self.bins = bins[:self.d]
		self.ranges = []
		if len(ranges)==1:
			self.ranges = [ranges for a in range(self.d)]
		elif len(ranges)>=self.d:
			self.ranges = ranges[:self.d]

	def copy(self):
		name = self.name
		d = self.d
		bins = self.bins
		ranges = self.ranges
		axes = self.axes
		newH = Histdd(name,d,bins,ranges,axes)
		newH.fresh = self.fresh
		newH.ticks = self.ticks
		newH.savename = self.savename
		newH.H = self.H
		
		return newH

	def fill_n(self,data,weight=None):
		if self.fresh:
			self.H,self.edges = np.histogramdd(data,bins=self.bins,range=self.ranges,weights=weight)
			if len(self.ticks)==0:
				self.ticks = [tfe(ed) for ed in self.edges]
			if len(self.ranges)==0:
				self.ranges = [[ed[0],ed[-1]] for ed in self.edges]
			self.fresh = False
		else:
			self.H += np.histogramdd(data,bins=self.edges,range=self.ranges,weights=weight)[0]
			
		if weight!=None:
			self.nEntries += np.sum(weight)
		else:
			self.nEntries += len(data[0])
		
		return True
	
	def mavg(self,ar,weights=None,axis=None,returned=False):
		a = np.asanyarray(ar)
		if weights is None:
			avg = np.nanmean(a,axis)
			scl = avg.dtype.type(a.size/avg.size)
		else:
			wgt = np.asanyarray(weights)
			if issubclass(a.dtype.type, (np.integer, np.bool_)):
				result_dtype = np.result_type(a.dtype, wgt.dtype, 'f8')
			else:
				result_dtype = np.result_type(a.dtype, wgt.dtype)

			# Sanity checks
			if a.shape != wgt.shape:
				if axis is None:
					raise TypeError(
						"Axis must be specified when shapes of a and weights "
						"differ.")
				if wgt.ndim != 1:
					raise TypeError(
						"1D weights expected when shapes of a and weights differ.")
				if wgt.shape[0] != a.shape[axis]:
					raise ValueError(
						"Length of weights not compatible with specified axis.")

				# setup wgt to broadcast along axis
				wgt = np.broadcast_to(wgt, (a.ndim-1)*(1,) + wgt.shape)
				wgt = wgt.swapaxes(-1, axis)

			#mask = np.isnan(a)*np.isnan(wgt)
			#a = a*mask
			#wgt = wgt*mask
			scl = np.sum(a,axis=axis, dtype=result_dtype)
			#if np.any(scl == 0.0):
			#	raise ZeroDivisionError(
			#		"Weights sum to zero, can't be normalized")

			avg = np.sum(np.multiply(a, wgt, dtype=result_dtype),axis=axis)
			avg = np.nan_to_num(avg/scl)
		if returned:
			if scl.shape != avg.shape:
				scl = np.broadcast_to(scl, avg.shape).copy()
			return avg, scl,wgt
		else:
			return avg
	
	def merr(self,a,wgt=None,ax=None):
		avg,scl,mwgt = self.mavg(a, weights=wgt,axis=ax,returned=True)
		# Fast and numerically precise:
		err2 = np.sqrt(a)
		err3 = np.ma.masked_invalid(err2*mwgt/scl[...,np.newaxis])
		err4 = np.ma.masked_invalid(np.nansum(a*mwgt)/(scl**2))
		err4 = err2*err4[...,np.newaxis]
		err = err3-err4
		err = np.sum(err**2,axis=ax)
		return np.sqrt(err)
	
	def plot3d(self,lims=[],norm=False,err=False,utickx=[],uticky=[],inv=False):
		fig = plt.figure()
		mult = 1
		vals = self.mavg(self.H,axis=2,weights=self.ticks[2])
		if norm:
			mult = np.ma.masked_invalid(1.0/np.nanmean(vals))
		
		if err:
			ax = fig.add_subplot(121)
		else:
			ax = fig.add_subplot(111)
		
		vals = mult*np.transpose(vals)
		plt.pcolormesh(self.ticks[0],self.ticks[1],vals)
		if inv:
			ax.invert_yaxis()
		if len(utickx)>0:
			plt.xticks(utickx)
		if len(uticky)>0:
			plt.yticks(uticky)
		cbar = plt.colorbar()
		clab = ''
		if len(self.axes)>0:
			plt.xlabel(self.axes[0])
		if len(self.axes)>1:
			plt.ylabel(self.axes[1])
		if len(self.axes)>2:
			clab = self.axes[2]
			cbar.ax.set_ylabel(clab)
			
		if err:
			vals = mult*np.transpose(self.merr(self.H,wgt=self.ticks[2],ax=2))
			ax2 = fig.add_subplot(122)
			clab += ' error'
			plt.pcolormesh(self.ticks[0],self.ticks[1],vals)
			if inv:
				ax2.invert_yaxis()
			if len(utickx)>0:
				plt.xticks(utickx)
			if len(uticky)>0:
				plt.yticks(uticky)
			cbar2 = plt.colorbar()
			cbar2.ax.set_ylabel(clab)
			if len(self.axes)>0:
				plt.xlabel(self.axes[0])
			if len(self.axes)>1:
				plt.ylabel(self.axes[1])
		fig.show
		return fig

	def plot2d(self,lims=[]):
		plt.pcolormesh(self.ticks[0],self.ticks[1],np.transpose(self.H))
		cbar = plt.colorbar()
		if len(self.axes)>0:
			plt.xlabel(self.axes[0])
		if len(self.axes)>1:
			plt.ylabel(self.axes[1])
		if len(self.axes)>2:
			clab = self.axes[2]
			cbar.ax.set_ylabel(clab)
		plt.show
		return plt.gcf()

	def plot1d(self,lims=[]):
		plt.plot(self.ticks[0],self.H)
		#cbar = plt.colorbar()
		if len(self.axes)>0:
			plt.xlabel(self.axes[0])
		plt.show
		return plt.gcf()

	def plot(self,lims=[]):
		if self.d==1:
			return True,self.plot1d(lims)
		if self.d==2:
			return True,self.plot2d(lims)
		if self.d==3:
			return True,self.plot3d(lims)
		else:
			return False,

	def plts(self,savedir='./',lims=[]):
		plt.clf()
		fig = self.plot(lims)
		if fig[0]:
			if self.savename=='' or savedir!='./':
				self.savename = savedir+self.name
			fig[1].savefig(self.savename)
			return fig[1]
		
		
	def collapseAxis(self,axis=None):
		if axis==None:
			axis=self.d-1
		elif axis>self.d-1:
			print('axis {} not present in {}-dimensional plot'.format(axis,self.d))
			return None
		nd = self.d-1
		nH = self.mavg(self.H,axis=axis,weights=self.ticks[axis])
		nranges = self.ranges
		nranges.pop(axis)
		nbins = self.bins
		nbins.pop(axis)
		nedges = self.edges
		nticks = self.ticks
		naxes = self.axes
		if len(naxes)>nd:
			naxes.pop(axis)
		
		nHist = Histdd(self.name,nd,bins=nbins,ranges=nranges,axes=naxes)
		nHist.H = nH
		nticks.pop(axis)
		nHist.ticks = nticks
		nedges.pop(axis)
		nHist.edges = nedges
		nHist.fresh = False
		
		return nHist
		
	def toTHist(self):
		h = None
		if self.d==1:
			h = TH1F(self.name,self.name,self.bins[0],self.ranges[0][0],self.ranges[0][1])
			for binx in range(self.bins[0]):
				h.SetBinContent(binx,self.H[binx])
		elif self.d==2:
			h = TH2F(self.name,self.name,self.bins[0],self.ranges[0][0],self.ranges[0][1],self.bins[1],self.ranges[1][0],self.ranges[1][1])
			for binx in range(self.bins[0]):
				for biny in range(self.bins[1]):
					h.SetBinContent(binx,biny,self.H[binx][biny])
		elif self.d==3:
			h = TH3F(self.name,self.name,self.bins[0],self.ranges[0][0],self.ranges[0][1],self.bins[1],self.ranges[1][0],self.ranges[1][1],self.bins[2],self.ranges[2][0],self.ranges[2][1])
			for binx in range(self.bins[0]):
				for biny in range(self.bins[1]):
					for binz in range(self.bins[2]):
						h.SetBinContent(binx,biny,binz,self.H[binx][biny][binz])
		if h!=None:
			if len(self.axes)>0 and self.d>0:
				h.GetXaxis().SetTitle(self.axes[0])
			if len(self.axes)>1 and self.d>1:
				h.GetYaxis().SetTitle(self.axes[1])
			if len(self.axes)>2 and self.d>2:
				h.GetZaxis().SetTitle(self.axes[2])
		return h
	
