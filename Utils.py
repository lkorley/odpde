import uproot as up
import numpy as np
import awkward as awk
import matplotlib as mpl
import physt
import Histdd
mpl.use('Agg')
import matplotlib.pyplot as plt
from pylab import rcParams
rcParams['figure.figsize'] = 22, 11
rcParams['font.size'] = 24
rcParams["savefig.format"] = 'png'
#rcParams['text.usetex'] = True

import sys
import os
import pickle

from ROOT import TH1F,TH2F,TH3F,TFile

def iterable(obj):
	try:
		iter(obj)
	except Exception:
		return False
	else:
		return True

def tfe(edges):
	diff = edges[1:] - edges[:-1]
	ticks = edges[:-1] + diff/2.0
	return ticks
#define histogram class: should store range, bincount, title (axis and plot)
#class should automate appending bins, just needs to be passed the right set of arrays

EnRange = [0,100]
yieldRange = [0,15]
PDERange = [0,0.5]
chPDERange = [0,0.2]
PhRange = [0,1000]
PhRange2 = [0,200]
xRange = [-1700,1700]
yRange = [-1700,1700]
zRange = [-1200,3000]
r2Range = [0,1700*1700]
rRange = [0,1700]
EnLab = '$EDep~(keV)$'
rawphLab = '#Photons Emmited'
hitphLab = '#Photons Detected'
tpdelab = 'MCTruth PDE'
pdelab = 'PDE'
tyieldlab = 'yield $(photons/keV)$'
thityieldlab = 'hit yield $(photons/keV)$'
r2lab = '$R^2~(mm^2)$'
rlab = '$R~(mm)$'
thetalab = '$\Theta~(\Pi)$'
xlab = 'X (mm)'
ylab = 'Y (mm)'
zlab = 'Z (mm)'
Em = 1.0/0.127
Ec = 12.77/0.127

pmtpositions = np.reshape(np.fromfile('ODPMTPos.txt',dtype=np.float64,sep=' '),(120,4))[:,1:]
pmtz = pmtpositions[:,2]
pmtTheta = np.arctan2(pmtpositions[:,1],pmtpositions[:,0])/np.pi

def createhdicts():
	outdict = {'pdeXYZ' : Histdd.Histdd('TPDEXYZ',d=4,bins=[220,220,220,200],ranges=[xRange,yRange,zRange,PDERange],axes=[xlab,ylab,zlab,tpdelab])
			 }

	return outdict
	
def createbranchdict():
	trubranches = {b'mcTruthVertices.energyDep_keV':None, 
				   b'mcTruthVertices.rawScintPhotons':None,
				   b'mcTruthVertices.scintPhotonHits':None,
				   b'mcTruthVertices.detectedScintPhotons':None,
				   b'mcTruthEvent.parentPositionX_mm':None,
				   b'mcTruthEvent.parentPositionY_mm':None,
				   b'mcTruthEvent.parentPositionZ_mm':None}
	return trubranches

def writeHists(outdict,pickleout,figdir,sep=False):
	for his in outdict.values():
		his.plts(savedir=figdir)
	print('Writing histos')
	if sep:
		for his in outdict.keys():
			pickle.dump({his:outdict[his]},open(pickleout[:-2]+'_{}.p'.format(his),'wb'),protocol=4)
	else:
		pickle.dump(outdict,open(pickleout,"wb"),protocol=4)
	
def printSums(outdict):
	for his in outdict.keys():
		print('{}:{} Entries'.format(his,outdict[his].nEntries))

def eventana(truthevt,trubranches,outdict,nFi,nEvs):
	nFi += 1
	nEvs += len(truthevt[b'mcTruthEvent.parentPositionY_mm'].flatten())
	print('processing chunk '+str(nFi)+' || '+str(nEvs)+' Evts so far')
		
	for branch in trubranches.keys():
		trubranches[branch] = awk.fromiter(truthevt[branch])
	
	tLCE = trubranches[b'mcTruthVertices.scintPhotonHits'].sum()/trubranches[b'mcTruthVertices.rawScintPhotons'].sum()
	tPDE = trubranches[b'mcTruthVertices.detectedScintPhotons'].sum()/trubranches[b'mcTruthVertices.rawScintPhotons'].sum()

	outdict['pdeXYZ'].fill_n(( trubranches[ b'mcTruthEvent.parentPositionX_mm' ].flatten().flatten(), trubranches[ b'mcTruthEvent.parentPositionY_mm'].flatten().flatten(),trubranches[ b'mcTruthEvent.parentPositionZ_mm' ].flatten().flatten(),tPDE.flatten().flatten()))
	
	return nFi,nEvs

def eventloop(truthfilelist,figdir='./figs/',pickleout = '',esteps = 5000,sep=False):
	nTot = len(truthfilelist)
	nFi = 0
	nEvs=0
	#loop through uproot iterator
	outdict = createhdicts()
	trubranches = createbranchdict()
	for truthevt in up.iterate(truthfilelist,'RQMCTruth',trubranches.keys(),entrysteps=esteps):
		nFi,nEvs=eventana(truthevt,trubranches,outdict,nFi,nEvs)
		#print(nFi)
		
	if nFi>0 and pickleout!='':
		writeHists(outdict,pickleout,figdir,sep)
	else:
		print('Nothing to pickle')
	return outdict

